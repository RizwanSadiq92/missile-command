﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    #region Data

    [SerializeField] private float m_BulletSpeed;
    [SerializeField] private Rigidbody m_Rigid;

    public float BulletDestoryTime;
    public float BulletExplodeRadius;

    private Turret m_Turret;
    public Turret CurrenTurret { set { m_Turret = value; } }

    #endregion Data

    #region Unity Initialization

    private void OnEnable()
    {
        GameManagerDelegate.OnLevelComplete += OnReset;
        GameManagerDelegate.OnLevelFail += OnReset;
        GameManagerDelegate.OnLevelReset += OnReset;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnLevelComplete -= OnReset;
        GameManagerDelegate.OnLevelFail -= OnReset;
        GameManagerDelegate.OnLevelReset -= OnReset;
    }

    #endregion 


    public void FireBullet(Vector3 i_Target)
    {
        StartCoroutine(WaitForTargetReached(i_Target));
    }

    IEnumerator WaitForTargetReached(Vector3 i_Target)
    {
        
        var distance = Vector3.Distance(transform.position,i_Target);

        while(distance > 0.1f)
        {
            distance = Vector3.Distance(transform.position, i_Target);
            transform.position = Vector3.MoveTowards(transform.position, i_Target, Time.deltaTime * m_BulletSpeed);
            yield return null;
        }

        if(m_Turret != null)
        {
            m_Turret.CoolDownFinish();
        }

        var bulletSize = this.transform.localScale.x;

        while(bulletSize < BulletExplodeRadius - 0.1f)
        {
            bulletSize = Mathf.Lerp(bulletSize, BulletExplodeRadius, Time.deltaTime * 10f);
            this.transform.localScale = new Vector3(bulletSize,bulletSize,bulletSize);
            yield return null;
        }

        yield return new WaitForSeconds(BulletDestoryTime);
        Destroy(this.gameObject);

    }

    void OnReset()
    {
        StopAllCoroutines();
        Destroy(this.gameObject);
    }
}
