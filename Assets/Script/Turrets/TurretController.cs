﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    #region Data

    public static TurretController Instance;

    [SerializeField] Turret[] m_Turret;

    #endregion Data

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }


    public void Fire(Vector3 i_Target, GameObject i_AimPointer)
    {
        var currentTurret = checkTurret();
        if(currentTurret != null)
        {
            currentTurret.Fire(i_Target, i_AimPointer);
        }
        else
        {
            Debug.Log("No Turret Available");
        }
    }

    Turret checkTurret()
    {
        for(int i=0; i< m_Turret.Length; i++)
        {
            if (!m_Turret[i].IsTurretBusy())
            {
                return m_Turret[i];
            }
        }

        return null;
    }

    public bool IsTurretFree()
    {
        if (checkTurret() != null)
            return true;
        else
            return false;
    }
}
