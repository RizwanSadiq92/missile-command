﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{

    [SerializeField] private GameObject m_Bullet;
    [SerializeField] private Transform m_FirePosition;
    private bool m_CanFire = true;
    private GameObject m_TargetPointer;

    private void OnEnable()
    {
        GameManagerDelegate.OnLevelReset += CoolDownFinish;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnLevelReset -= CoolDownFinish;
    }

    public void Fire(Vector3 i_Target, GameObject i_AimPointer)
    {
        if (m_CanFire)
        {
            var bullet = Instantiate(m_Bullet);
            bullet.transform.position = m_FirePosition.position;
            bullet.GetComponent<Bullet>().FireBullet(i_Target);
            bullet.GetComponent<Bullet>().CurrenTurret = this;
            m_CanFire = false;
            m_TargetPointer = i_AimPointer;
        }
    }

   public void CoolDownFinish()
    {
        m_CanFire = true;
        m_TargetPointer.SetActive(!m_CanFire);
    }

    public bool IsTurretBusy()
    {
        return !m_CanFire;
    }
}
