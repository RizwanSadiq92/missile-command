﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    #region Data

    public static GameManager Instance;
    public bool HasLevelStarted;

    #endregion Data

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }


    public void LevelStarted()
    {
        GameManagerDelegate.StartLevel();
        HUDManager.Instance.SetTotalEnemies();
        HasLevelStarted = true;
    }

    public void LevelComplete()
    {
        if (!HasLevelStarted)
            return;
        GameManagerDelegate.LevelComplete();
        HasLevelStarted = false;
    }

    public void LevelFail()
    {
        if (!HasLevelStarted)
            return;
        GameManagerDelegate.LevelFail();
        HasLevelStarted = false;
    }

    public void ResetLevel()
    {
        GameManagerDelegate.LevelReset();
        HasLevelStarted = false;
    }

}
