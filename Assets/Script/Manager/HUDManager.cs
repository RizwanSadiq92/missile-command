﻿
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    #region Data

    public static HUDManager Instance;

    [SerializeField] private Transform[] m_AimPointer;

    [SerializeField] private GameObject[] m_AllPanels;
    [Space]
    [Header("LevelStart")]
    [SerializeField] private GameObject m_LevelStartPanel;
    [SerializeField] private Button m_LevelStartButton;

    [Space]
    [Header("LevelComplete")]
    [SerializeField] private GameObject m_LevelComplete;
    [SerializeField] private Button m_RestartButton;

    [Space]
    [Header("LevelComplete")]
    [SerializeField] private GameObject m_LevelFail;
    [SerializeField] private Button m_RetryButton;

    [Space]
    [Header("Scoring")]
    [SerializeField] private TextMeshProUGUI m_Score;
    private int m_TotalScore;


    [SerializeField] private TextMeshProUGUI m_TotalEnemies;

    #endregion Data

    #region Unity Initialization
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }

    private void OnEnable()
    {
        m_LevelStartButton.onClick.AddListener(StartButtonClicked);
        m_RestartButton.onClick.AddListener(Retry);
        m_RetryButton.onClick.AddListener(Retry);

        GameManagerDelegate.OnLevelComplete += OpenCompletePanel;
        GameManagerDelegate.OnLevelFail += OpenFailPanel;
        GameManagerDelegate.OnLevelReset += onResetGame;

        SetTotalEnemies();
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnLevelComplete -= OpenCompletePanel;
        GameManagerDelegate.OnLevelFail -= OpenFailPanel;
        GameManagerDelegate.OnLevelReset -= onResetGame;

    }

    #endregion 

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && TurretController.Instance.IsTurretFree() && GameManager.Instance.HasLevelStarted)
        {
            setAimPosition(Input.mousePosition);
        }
    }

    #region Aim Pointer

    void setAimPosition(Vector3 i_ScreenPos)
    {

        var currentAimPointer = GetAimPointer();
        if (currentAimPointer != null)
        {
            currentAimPointer.gameObject.SetActive(true);
            currentAimPointer.position = i_ScreenPos;
        }

        var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0;

        TurretController.Instance.Fire(mousePosition, currentAimPointer.gameObject);
    }

    Transform GetAimPointer()
    {
        for(int i=0; i< m_AimPointer.Length; i++)
        {
            if (!m_AimPointer[i].gameObject.activeSelf)
            {
                return m_AimPointer[i];
            }
        }
        return null;
    }

    void closeAllPointer()
    {
        for (int i = 0; i < m_AimPointer.Length; i++)
        {
            m_AimPointer[i].gameObject.SetActive(false);
        }
    }

    #endregion Aim Pointer

    void closeAll()
    {
        for(int i=0; i< m_AllPanels.Length; i++)
        {
            m_AllPanels[i].SetActive(false);
        }
    }

    #region Level Start

    public void OpenStartPanel()
    {
        closeAll();
        m_LevelStartPanel.SetActive(true);
    }

    public void StartButtonClicked()
    {
        closeAll();
        GameManager.Instance.LevelStarted();
    }

    #endregion Level Start

    #region Level Complete

    public void OpenCompletePanel()
    {
        closeAll();
        m_LevelComplete.SetActive(true);
    }

    public void Retry()
    {
        closeAll();
        GameManager.Instance.ResetLevel();
        OpenStartPanel();
    }

    #endregion Level Complete

    #region Level Fail

    public void OpenFailPanel()
    {
        closeAll();
        m_LevelFail.SetActive(true);
    }

    #endregion Level Start

    #region Scoring

    public void AddScore(int i_RewardScore)
    {
        m_TotalScore += i_RewardScore;
        m_Score.text = "Score " + m_TotalScore;
    }
    private void ClearScore()
    {
        m_TotalScore = 0;
        AddScore(m_TotalScore);
        
    }

    #endregion Scoring

    #region Enemy Count
    public void SetTotalEnemies()
    {

        m_TotalEnemies.text ="Enemy Left:- " + AlienMotherShip.Instance.GetTotalEnemies();

    }

    private void ResetEnemies()
    {
        m_TotalEnemies.text = "Enemy Left:- " + 0;
    }

    #endregion

    void onResetGame()
    {
        closeAllPointer();
        ClearScore();
        ResetEnemies();
    }
}
