﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHealth : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Alien")
        {
            DestroyBase();
        }
    }

    void DestroyBase()
    {
        BaseController.Instance.DecreaseActiveBase();
        this.gameObject.SetActive(false);
    }

    void OpenBase()
    {
        this.gameObject.SetActive(true);
    }
}
