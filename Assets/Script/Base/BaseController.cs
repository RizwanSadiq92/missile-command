﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    #region Data

    public static BaseController Instance;

    [SerializeField] private Transform[] m_AllBases;
    [SerializeField] private int totalBaseActive;
    public List<Transform> m_ActiveBases;

    #endregion Data

    #region Unity Initialization

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        setTotalBase();
    }

    private void OnEnable()
    {
        GameManagerDelegate.OnLevelReset += onGameReset;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnLevelReset = onGameReset;
    }

    #endregion

    public Transform GetRandomBase()
    {
        CheckTotalActiveBases();
        return m_ActiveBases[Random.Range(0, m_ActiveBases.Count-1)];
    }

    void CheckTotalActiveBases()
    {
        m_ActiveBases = new List<Transform>();
        m_ActiveBases.Clear();
        for(int i = 0 ;i< m_AllBases.Length; i++)
        {
            if(m_AllBases[i].gameObject.activeSelf)
                m_ActiveBases.Add(m_AllBases[i]);
        }
    }

    void openAllBases()
    {
        for (int i = 0; i < m_AllBases.Length; i++)
        {
            m_AllBases[i].gameObject.SetActive(true);
        }
    }

    void setTotalBase()
    {
        totalBaseActive = m_AllBases.Length;
    }

    public bool IsBaseFinish()
    {
        if (totalBaseActive == 0)
            return true;
        else
            return false;
    }
    public void DecreaseActiveBase()
    {
        totalBaseActive--;
        if(totalBaseActive == 0)
        {
            GameManager.Instance.LevelFail();

        }
    }

    void onGameReset()
    {
        setTotalBase();
        openAllBases();
    }
}
