﻿
using UnityEngine;

public class AlienHealth : MonoBehaviour
{
    [SerializeField] private AlienMinions m_Aliens;
    [SerializeField] private int m_KillScore;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Bullet")
        {
            HUDManager.Instance.AddScore(m_KillScore);
            m_Aliens.KillAlien();
        }
    }
}
