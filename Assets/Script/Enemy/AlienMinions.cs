﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienMinions : MonoBehaviour
{
    #region Data

    [SerializeField] private float m_AlienSpeed;

    #endregion Data


    #region Unity Initialization

    private void OnEnable()
    {
        GameManagerDelegate.OnLevelComplete += KillAlien;
        GameManagerDelegate.OnLevelFail += KillAlien;
        GameManagerDelegate.OnLevelReset += KillAlien;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnLevelComplete -= KillAlien;
        GameManagerDelegate.OnLevelFail -= KillAlien;
        GameManagerDelegate.OnLevelReset -= KillAlien;
    }

    #endregion 

    public void DeployAlien(Vector3 i_Target)
    {
        StartCoroutine(WaitForTargetReached(i_Target));
    }

    IEnumerator WaitForTargetReached(Vector3 i_Target)
    {

        var distance = Vector3.Distance(transform.position, i_Target);

        while (distance > 0.1f)
        {
            distance = Vector3.Distance(transform.position, i_Target);
            transform.position = Vector3.MoveTowards(transform.position, i_Target, Time.deltaTime * m_AlienSpeed);
            yield return null;
        }


        KillAlien();

    }

    public void KillAlien()
    {
        StopAllCoroutines();
        
        AlienMotherShip.Instance.AlienMinionDestroyed();

        if (GameManager.Instance.HasLevelStarted)
        {
            HUDManager.Instance.SetTotalEnemies();
        }
        Destroy(this.gameObject);
    }
}
