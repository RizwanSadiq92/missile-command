﻿using UnityEngine;

public class AlienMotherShip : MonoBehaviour
{
    #region Data

    public static AlienMotherShip Instance;

    [SerializeField] private GameObject m_AlienShip;
    [SerializeField] private Transform[] m_SpawnPosition;

    [SerializeField] private float m_MaxCoolDownTime;
    private float m_CurrentCoolDownTime = 0;

    [SerializeField] private float m_StartDelay;

    [SerializeField] private int m_MaxAliens;
    private int m_TotalAliensDeployed;
    private int m_TotalKilled;


#endregion Data




    #region Unity Initialization

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
    private void OnEnable()
    {
        GameManagerDelegate.OnLevelStarted += StartDeploying;

        GameManagerDelegate.OnLevelFail += onReset;
        GameManagerDelegate.OnLevelReset += onReset;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnLevelStarted -= StartDeploying;

        GameManagerDelegate.OnLevelFail -= onReset;
        GameManagerDelegate.OnLevelReset -= onReset;
    }

    #endregion 

    void StartDeploying()
    {
        InvokeRepeating(nameof(Deploy), m_MaxCoolDownTime, 1);
    }

    void Deploy()
    {
        if (m_TotalAliensDeployed < m_MaxAliens)
        {

            var alienMinions = Instantiate(m_AlienShip);
            alienMinions.transform.position = m_SpawnPosition[Random.Range(0, m_SpawnPosition.Length - 1)].position;
            alienMinions.GetComponent<AlienMinions>().DeployAlien(BaseController.Instance.GetRandomBase().position);
            m_TotalAliensDeployed++;

        }
        else
        {
            CancelInvoke();
            
        }
    }

    public void AlienMinionDestroyed()
    {
        m_TotalKilled++;
        if(m_TotalKilled == m_MaxAliens)
        {
            GameManager.Instance.LevelComplete();
        }
    }

    public int GetTotalEnemies()
    {
        return m_MaxAliens - m_TotalKilled;
    }

    void onReset()
    {
        CancelInvoke();
        m_TotalAliensDeployed = 0;
        m_TotalKilled = 0;
    }


}
