﻿public class GameManagerDelegate
{
    public delegate void OnGameManagerDelegate();
    public static OnGameManagerDelegate OnLevelComplete;
    public static OnGameManagerDelegate OnLevelFail;
    public static OnGameManagerDelegate OnLevelStarted;
    public static OnGameManagerDelegate OnLevelReset;


    public static void StartLevel()
    {
        if(OnLevelStarted != null)
        {
            OnLevelStarted.Invoke();
        }
    }

    public static void LevelComplete()
    {
        if (OnLevelComplete != null)
        {
            OnLevelComplete.Invoke();
        }
    }

    public static void LevelFail()
    {
        if (OnLevelFail != null)
        {
            OnLevelFail.Invoke();
        }
    }

    public static void LevelReset()
    {
        if (OnLevelReset != null)
        {
            OnLevelReset.Invoke();
        }
    }
}
